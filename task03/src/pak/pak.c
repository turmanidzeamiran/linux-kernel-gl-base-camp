#include<stdio.h>
#include<stdlib.h>
#include<string.h>


void zip(char *filename,char *outfile)
{//Compression function 
	FILE *in, *out;
	int filelen;
	char cur,tmp;	
	if(!(in=fopen(filename,"rb")))
		printf("File open failed\n");
	else
	{
		out=fopen(outfile,"wb");
		cur=fgetc(in);
		tmp=cur;
		filelen=1;
		while(!feof(in))
		{
			cur=fgetc(in);
			if(cur==tmp){
				filelen++;
			}//Perform RLE compression 
			else{
				fputc(filelen+'0',out);
				fputc(tmp,out);
				tmp=cur;
				filelen=1;
			}
		}
	}
	fclose(in);
	fclose(out);
}

int main(int argc,char *argv[])
{

	printf("Pak: compress started\n");
	printf("[1]: %s\r\n", argv[1]);
	printf("[2]: %s\r\n", argv[2]);
	zip(argv[1], argv[2]);
	printf("compress finished\n");
	return 0;
}
