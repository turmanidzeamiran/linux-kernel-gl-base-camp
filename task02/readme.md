Task: write a Guess the Number game using the Bash script.
===========================================================
Subtask: create a random number generator script.
-------------------------------------------------
"New script: Added script with random number generation from 0 to 5"  
Subtask completed.


Subtask2: Extract Function refactoring.
---------------------------------------
"Edit sctipt: Added separate function for rand gen"  
Subtask2 completed...


Subtask3: Compare input with generated.
---------------------------------------
"Script change acording subtask 3"  
Subtask3 completed...


Subtask4: Check script input.
---------------------------------------
"Script extended according to subtask 4"  
Subtask4 completed...


